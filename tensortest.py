import tensorflow as tf
import numpy
import matplotlib.pyplot as plt
import csv

test_number = 30
num_input = 4
num_classes = 3
learning_rate = 0.2
batch_size = 10
display_step = 1

def getNextBatch(size):
    global data
    pop = data[:size]
    data = data[size:]
    x = pop[:, 0:4]
    yl = list(pop[:,4].astype(int))
    y = list()
    for row in yl:
        if(row == 1):
            y.append([1, 0, 0])
        elif(row == 2):
            y.append([0, 1, 0])
        elif(row == 3):
            y.append([0, 0, 1])
    y = numpy.asarray(y)
    return x, y

with open('Attribute_DataSet.csv', 'r') as csvfile:
    global flower_list
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    flower_list = list(spamreader)

data = numpy.asarray(flower_list[1:])
data = data.astype(float)
numpy.random.shuffle(data)
train_data = data[0: -test_number]
test_data = data[-test_number:]


## creating network
# input
a0 = tf.placeholder("float", [None, num_input], name="a0")
# ouput
y = tf.placeholder("float", [None, num_classes], name="y")
# weights
w =  tf.Variable(tf.random_normal([num_input, num_classes]))
# biases
b =  tf.Variable(tf.random_normal([num_classes]))

net = tf.add(tf.matmul(a0, w), b)
net = tf.nn.sigmoid(net, name ='sigmoid') 
loss_op = tf.losses.mean_squared_error(labels = y, predictions = net)
optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(loss_op)

correct_pred = tf.equal(tf.argmax(net, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

init = tf.global_variables_initializer()

# Start training
with tf.Session() as sess:

    # Run the initializer
    sess.run(init)

    for step in range(1, int(len(train_data) / batch_size)):
        batch_x, batch_y = getNextBatch(batch_size)
        # Run optimization op (backprop)
        sess.run(train_op, feed_dict={a0: batch_x, y: batch_y})
        if step % display_step == 0 or step == 1:
            # Calculate batch loss and accuracy
            loss, acc = sess.run([loss_op, accuracy], feed_dict={a0: batch_x,
                                                                 y: batch_y})
            print("Step " + str(step) + ", Minibatch Loss= " + \
                  "{:.4f}".format(loss) + ", Training Accuracy= " + \
                  "{:.3f}".format(acc))


    np = sess.run(net, feed_dict={a0: batch_x})
    print(np)
    print(batch_y)
            

    print("Optimization Finished!")

    # # Calculate accuracy for MNIST test images
    # print("Testing Accuracy:", \
    #     sess.run(accuracy, feed_dict={a0: mnist.test.images,
    #                                   y: mnist.test.labels}))