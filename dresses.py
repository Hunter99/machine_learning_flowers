#!/usr/bin/env python

import csv
import numpy
import random

def calculate_outcome(row, thetas):
    calculated_result = numpy.dot(thetas, row)
    return calculated_result

with open('Attribute_DataSet.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    flower_list = list(spamreader)
    flower_list = flower_list[1:]
    random.shuffle(flower_list)
    flower_teach_list = flower_list[:-40]
    flower_test_list = flower_list[-40:]
    thetas = [0.1, 0.1, 0.1, 0.1]

    theta_switch = 0

    flower_rows = flower_teach_list
    for flower_row in flower_rows:
        calc_row = flower_row[:-1]
        expected_result = float(flower_row[-1])
        calc_row = [float(x) for x in calc_row]
        calculated_result = calculate_outcome(calc_row, thetas)
        difference = expected_result - calculated_result
        thetas[theta_switch] = thetas[theta_switch] + difference/100
        theta_switch = (theta_switch + 1) % 4
        print(expected_result)
        print(calculated_result)
        print(thetas)

    print("------------------------------------------------------")

    right = 0
    wrong = 0
    flower_rows = flower_test_list[1:]
    for flower_row in flower_rows:
        calc_row = flower_row[:-1]
        expected_result = float(flower_row[-1])
        calc_row = [float(x) for x in calc_row]
        calculated_result = round(calculate_outcome(calc_row, thetas))
        if expected_result == calculated_result:
            right = right + 1
        else:
            wrong = wrong + 1
        print("expected_result " + str(expected_result))
        print("calculated_result " + str(calculated_result))
    print("right: " + str(right))
    print("wrong: " + str(wrong))

print(thetas)