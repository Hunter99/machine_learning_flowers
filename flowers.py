import csv
import numpy
import random

omegas = [0.1 for x in [0]*4]
package_size = 5
weight = 0.001

def calculate_outcome(row, omegas):
    calculated_result = numpy.dot(omegas, row)
    return calculated_result

with open('Attribute_DataSet.csv', 'rb') as csvfile:
    global flower_list
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    flower_list = list(spamreader)

flower_list = flower_list[1:]
#random.shuffle(flower_list)
flower_teach_list = flower_list[:-40]
flower_test_list = flower_list[-40:]
teach_row_number = len(flower_teach_list)
number_of_batches = int(teach_row_number / package_size)
for i in range(number_of_batches):
    diff_cost_sum = [0] * 4
    for j in range(package_size):
        number = i * package_size + j
        calc_row = flower_teach_list[number][:-1]
        calc_row = [float(x) for x in calc_row]
        expected_value = float(flower_teach_list[number][-1])
        calculated_value = calculate_outcome(calc_row, omegas)
        factor = 2 * (calculated_value - expected_value)
        diff_cost = [x * factor for x in calc_row]
        for f, obj in enumerate(diff_cost):
            diff_cost_sum[f] = diff_cost_sum[f] + diff_cost[f]
    diff_cost_average = [x / package_size for x in diff_cost_sum]
    diff_cost_factor = [x * weight for x in diff_cost_average]
    print("============")
    print(omegas)
    print(diff_cost_factor)
    for f, obj in enumerate(diff_cost_factor):
        omegas[f] = omegas[f] - diff_cost_factor[f]

for row in flower_test_list:
    calc_row = row[:-1]
    calc_row = [float(x) for x in calc_row]
    expected_value = float(flower_teach_list[number][-1])
    calculated_value = calculate_outcome(calc_row, omegas)
    print(calculated_value - expected_value)

print(omegas)