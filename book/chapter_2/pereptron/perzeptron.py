import numpy as np

class Perzeptron():
    """ Perzeptron-Klassifizierer
    Parameters
    ----------
    eta : float   
        Lernrate zwischen (0.0 und 1.0)
    n_iter : int  
        Durchläufe der Trainingsdatenmengen
    random_state : int
        Startwert des Zufallszahlgenerators für Gewichtung 

    Attribute
    ---------
    __w : 1d-array
        Gewichtungen nach Anpassung
    __errors : list
        Anzahl der Fehlklassifizierungen (Updates) pro Epoche
    """

    def __init__(self, eta = 0.01, n_iter = 50, random_state = 1):
        self.eta = eta
        self.n_iter = n_iter
        self.random_state = random_state

    def fit(self, X, y):
        """ Anpassen an die Trainingsdaten 
        Parameter
        ---------
        X : Matrix, shape = [n_samples, n_features]
            Trainingsvektoren
        y : Vektor, shape = [n_samples]
            Zielwerte

        Rückgabewert
        ------------
        self : object
        """

        rgen = np.random.RandomState(self.random_state)
        self.__w = rgen.normal(0.0, 0.01, 1 + X.shape[1])   # 1 : loc, 2 : scale, 3 : size
        self.__errors = []

        for i in range(self.n_iter):
            error = 0
            for xi, target in zip(X, y):
                update = self.eta * (target - self.predict(xi))
                self.__w[1:] += update * xi
                self.__w[0] += update
                error += int(update != 0.0)
            self.__errors.append(error)
        return self

    def net_input(self, X):
        """ Nettoeingabe berechnen
        Parameter
        ---------
        X : Matrix, shape = [n_samples, n_features]

        Rückgabewert
        ------------
        self : object
        """
        return np.dot(X, self.__w[1:]) + self.__w[0]

    def predict(self, X):
        """ Klassenbezeichnung zurück geben
        Parameter
        ---------
        X : Matrix, shape = [n_samples, n_features]
        
        Rückgabewert
        ------------
        self : object
        """
        # Klassenbezeichnung wir hier nach der Berechnung durchgeführt.
        return np.where(self.net_input(X) >= 0.0, 1, -1)

    def get_errors(self):
        return self.__errors