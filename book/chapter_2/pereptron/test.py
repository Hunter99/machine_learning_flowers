import pandas as pd
import matplotlib.pyplot as plt
from perzeptron import *


df = pd.read_csv('Attribute_DataSet.csv', header=0)
for index, row in df.iterrows():
    if row[4] == 1:
        df.at[index, 'Species'] = 1
    else:
        df.at[index, 'Species'] = -1

df_sorted = df.sort_values(by = ['Species'])
ones = df_sorted.values[100:150]
others = df_sorted.values[0:100]

# plt.scatter(ones[:, 0], ones[:, 2], color = 'red', marker = 'o', label = 'setosa')
# plt.scatter(others[:, 0], others[:, 2], color = 'blue', marker = 'x', label = 'others')
# plt.legend()
# plt.show()

df_randomized = df.sample(frac = 1)
df_dropped = df_randomized.drop(['Sepal width', 'Petal width'], axis = 1)
values = df_dropped.values
X = values[0:100, 0:2]
y = values[0:100, 2]
X_test = values[100:150, 0:2]
y_test = values[100:150, 2]

pc = Perzeptron(eta = 0.1, n_iter = 10)
pc.fit(X, y)

# plt.plot(range(1, len(pc.get_errors()) + 1), pc.get_errors(), marker = 'o')
# plt.show()

y_predicted = pc.predict(X_test)
for pack in list(zip(y_predicted, y_test)):
    if pack[0] != pack[1]:
        print("Fehler")