import os
import sys
import cv2
from random import shuffle
import random
import numpy as np
from keras.utils.np_utils import to_categorical
import tensorflow as tf
import matplotlib.pyplot as plt
from keras.models import load_model
from keras.utils import CustomObjectScope
from keras.initializers import glorot_uniform

os.chdir('UTKFace')
files = os.listdir()
shuffle(files)
ages = [i.split('_')[0] for i in files]
sexes = [i.split('_')[1] for i in files]

print(len(ages))

# filter far objects
ages2 = ages.copy()
i = 0
for age in ages2:
    age = int(age)
    if age < 10 or age > 25:
        chance = random.randint(0, 100)
        if chance < 90:
            ages.pop(i)
            sexes.pop(i)
            files.pop(i)
            i -= 1
    i += 1

del ages2

print(len(ages))

classes = []
for i in ages:
    i = int(i)
    if i < 18:
        classes.append(0)
    if (i>=18):
        classes.append(1)

X_data = []
for f in files:
    face = cv2.imread(f)
    face = cv2.resize(face, (32, 32))
    X_data.append(face)

X = np.squeeze(X_data)

X = X.astype('float32')
X /= 255

categorical_labels = to_categorical(classes, num_classes=2)

size = len(X)

part_1 = int(size / 4 * 2)
part_2 = int(size / 4 * 1)

x_train, y_train = (X[:part_1], categorical_labels[:part_1])
x_test, y_test = (X[part_1:part_1+part_2], categorical_labels[part_1:part_1+part_2])
x_valid , y_valid = (X[part_1+part_2:], categorical_labels[part_1+part_2:])

if not os.path.isfile('../face_model_two_ages.h5'):
    model = tf.keras.Sequential()

    # Must define the input shape in the first layer of the neural network
    model.add(tf.keras.layers.Conv2D(filters=64, kernel_size=2, padding='same', activation='relu', input_shape=(32,32,3))) 
    model.add(tf.keras.layers.MaxPooling2D(pool_size=2))
    model.add(tf.keras.layers.Dropout(0.3))

    model.add(tf.keras.layers.Conv2D(filters=32, kernel_size=2, padding='same', activation='relu'))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=2))
    model.add(tf.keras.layers.Dropout(0.3))

    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(256, activation='relu'))
    model.add(tf.keras.layers.Dropout(0.5))
    model.add(tf.keras.layers.Dense(2, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    model.fit(x_train, y_train, batch_size=64, epochs=50, validation_data=(x_valid, y_valid),)

    model.save('../face_model_two_ages.h5')


else:
    with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
        model = load_model('../face_model_two_ages.h5')

labels =["CHILD",  # index 0
        "ADULT",     # index 2 
        ]



x_filtered = []
y_filtered = []
ages_filtered = []

for i, y in enumerate(ages[part_1:part_1+part_2]):
    y = int(y)
    if y > 13 and y < 18:
        y_filtered.append(y_test[i])
        x_filtered.append(x_test[i])
        ages_filtered.append(y)

x_filtered = np.array(x_filtered)
y_filtered = np.array(y_filtered)

y_hat = model.predict(x_filtered)


# Plot a random sample of 10 test images, their predicted labels and ground truth

figure = plt.figure(figsize=(20, 8))
for i in range(15):
    ax = figure.add_subplot(3, 5, i + 1, xticks=[], yticks=[])
    # Display each image
    ax.imshow(np.squeeze(x_filtered[i]))
    predict_index = np.argmax(y_hat[i])
    true_index = np.argmax(y_filtered[i])
    # Set the title for each image
    ax.set_title("{} ({}) {}".format(labels[predict_index], labels[true_index], ages_filtered[i]), color=("green" if predict_index == true_index else "red"))
plt.show()
