import os
import cv2
from random import shuffle
import numpy as np
from keras.utils.np_utils import to_categorical
import tensorflow as tf
import matplotlib.pyplot as plt
from keras.models import load_model
from keras.utils import CustomObjectScope
from keras.initializers import glorot_uniform

os.chdir('UTKFace')
files = os.listdir()
shuffle(files)
ages = [i.split('_')[0] for i in files]
sexes = [i.split('_')[1] for i in files]

classes = []
for i in ages:
    i = int(i)
    if i <= 14:
        classes.append(0)
    if (i>14) and (i<=25):
        classes.append(1)
    if (i>25) and (i<40):
        classes.append(2)
    if (i>=40) and (i<60):
        classes.append(3)
    if i>=60:
        classes.append(4)

X_data = []
for f in files:
    face = cv2.imread(f)
    face = cv2.resize(face, (32, 32))
    X_data.append(face)

X = np.squeeze(X_data)

X = X.astype('float32')
X /= 255

categorical_labels = to_categorical(classes, num_classes=5)

(x_train, y_train), (x_test, y_test) = (X[:15008], categorical_labels[:15008]) , (X[15008:], categorical_labels[15008:])
(x_valid , y_valid) = (x_test[:7000], y_test[:7000])
(x_test, y_test) = (x_test[7000:], y_test[7000:])

if not os.path.isfile('../face_model_five_ages.h5'):
    model = tf.keras.Sequential()

    # Must define the input shape in the first layer of the neural network
    model.add(tf.keras.layers.Conv2D(filters=64, kernel_size=2, padding='same', activation='relu', input_shape=(32,32,3))) 
    model.add(tf.keras.layers.MaxPooling2D(pool_size=2))
    model.add(tf.keras.layers.Dropout(0.3))

    model.add(tf.keras.layers.Conv2D(filters=32, kernel_size=2, padding='same', activation='relu'))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=2))
    model.add(tf.keras.layers.Dropout(0.3))

    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(256, activation='relu'))
    model.add(tf.keras.layers.Dropout(0.5))
    model.add(tf.keras.layers.Dense(5, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    model.fit(x_train, y_train, batch_size=64, epochs=25, validation_data=(x_valid, y_valid),)

    model.save('../face_model_five_ages.h5')


else:
    with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
        model = load_model('../face_model_five_ages.h5')

labels =["CHILD",  # index 0
        "YOUTH",      # index 1
        "ADULT",     # index 2 
        "MIDDLEAGE",        # index 3 
        "OLD",         # index 4
        ]

y_hat = model.predict(x_test)

# Plot a random sample of 10 test images, their predicted labels and ground truth
figure = plt.figure(figsize=(20, 8))
for i, index in enumerate(np.random.choice(x_test.shape[0], size=15, replace=False)):
    ax = figure.add_subplot(3, 5, i + 1, xticks=[], yticks=[])
    # Display each image
    ax.imshow(np.squeeze(x_test[index]))
    predict_index = np.argmax(y_hat[index])
    true_index = np.argmax(y_test[index])
    # Set the title for each image
    ax.set_title("{} ({})".format(labels[predict_index], labels[true_index]), color=("green" if predict_index == true_index else "red"))
plt.show()
